use regex::Regex;
use std::error::Error;
use std::{
	fs::File,
	io::{BufRead, BufReader},
};

extern crate walkdir;
use walkdir::DirEntry;

extern crate indexmap;
use indexmap::map::IndexMap;

#[derive(Default, Debug)]
pub struct FileLinesMatches {
	// Necessary due to lifetime constraints of the
	// String(s) representing the contents of the file in question
	file_path: String,
	lines: IndexMap<usize, String>,
	regexp: String,
}

impl FileLinesMatches {
	// pub fn get_regexp(&self) -> &str { &self.regexp.as_str() } // Not used so ignoring dead code warning
	pub fn get_file_path(&self) -> &str {
		&self.file_path.as_str()
	}
	pub fn get_matching_lines(&self) -> &IndexMap<usize, String> {
		&self.lines
	}
}

//clippy-allow b/c no file.path().to_string() function available
#[allow(clippy::useless_format)]
pub fn find_contiguous_lines_matching<'a>(
	file: &'a DirEntry,
	regexp: &str,
	minimum_lines: usize,
	verbosity_level: u64,
) -> Result<FileLinesMatches, Box<dyn Error>> {
	// matches => Object to be created and returned by function invocation
	let mut matches: FileLinesMatches = FileLinesMatches {
		file_path: format!(
			"{}",
			file.path()
				.to_str()
				.expect("Unable to convert file's path into a String!")
		),
		lines: IndexMap::new(),
		regexp: regexp.to_string(),
	};

	// Don't re-create/re-compile regex per line iteration
	let regex: Regex = Regex::new(regexp)?;
	// Local variable/storage to keep track of consecutive lines w/matching regex
	let mut temp: IndexMap<usize, String> = IndexMap::new();

	for (line_num, line) in BufReader::new(File::open(file.path()).expect("Unable to read file!"))
		.lines()
		.enumerate()
	{
		let line_str: String = line.expect("Unable to read line into String!");

		if verbosity_level >= 3 {
			eprintln!("{}:{}:{}", file.path().display(), line_num, line_str);
		}

		if !regex.is_match(line_str.as_str()) {
			if temp.len() >= minimum_lines {
				matches.lines.extend(temp.to_owned());
				if verbosity_level >= 3 {
					eprintln!(
						"\tAbove line did not match regex: '{}'! Clearing temp hashmap!",
						regexp
					);
				}
			}
			temp.clear();
			continue;
		}

		if verbosity_level >= 3 {
			eprintln!("\tAbove line matched regex: '{}'!", regexp);
			eprintln!("\t\tTemp hashmap now has {} entries!", &temp.len());
		}
		temp.insert(line_num, line_str);
	}
	Ok(matches)
}
