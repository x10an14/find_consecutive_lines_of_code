use regex::Regex;
use std::error::Error;

extern crate walkdir;
use walkdir::{DirEntry, WalkDir};

extern crate structopt;
use structopt::{
	clap::AppSettings::{ColorAuto, ColoredHelp},
	StructOpt,
};

// Imports from local crate files
mod line_matching;
use line_matching::FileLinesMatches;

#[derive(Debug, StructOpt)]
// Ref. indoc TODO
//#[structopt(long_about = LONG_DESCRIPTION)]
#[structopt(setting(ColorAuto), setting(ColoredHelp), about)]
struct Cli {
	/// Verbose mode (-v, -vv, -vvv, etc.).
	#[structopt(short, long, parse(from_occurrences))]
	verbose: u64,
	/// Path to search.
	#[structopt(short = "p", long = "path", default_value = ".")]
	search_path: String,
	/// Regex pattern to match once per multiple contiguous lines.
	#[structopt(short = "r", long = "regex-pattern", default_value = r".* = .*")]
	regex_pattern: String,
	/// Minimum amount of consecutive/sequential/contiguous lines which have a match with the regex-pattern.
	#[structopt(short = "m", long = "minimum-lines", default_value = "3")]
	minimum_lines: usize,
	/// Regex pattern for file's full path to include.
	///
	/// Examples:
	/// For all files ending in *.cs:  "\.cs$", for all files under a sub-folder of name X:  ".*/X/*".
	#[structopt(short = "i", long = "path-include", default_value = r".*")]
	path_include_pattern: String,
	/// Regex pattern for file names to ignore.
	///
	/// Example: Ignore all files with substring "X" in their path: "X", ignore all files under specific directory: "DIRNAME/".
	#[structopt(short = "I", long = "path-ignore", default_value = r"")]
	path_exclude_pattern: String,
}

fn main() -> Result<(), Box<dyn Error>> {
	let cli = Cli::from_args();
	let verbosity_level = cli.verbose;
	if verbosity_level > 1 as u64 {
		eprint!("\npath_exclude_pattern:\t");
		dbg!(&cli.path_exclude_pattern);
		eprint!("path_include_pattern:\t");
		dbg!(&cli.path_include_pattern);
		eprint!("\nregex_pattern:\t");
		dbg!(&cli.regex_pattern);
		eprint!("minimum_lines:\t");
		dbg!(&cli.minimum_lines);
		eprintln!("\n\nFollowing files were found:");
	}

	// For-loop for future potential parallization
	// purposes instead of builder pattern:
	let include_regexp: Regex = Regex::new(&cli.path_include_pattern)?;
	let exclude_regexp: Regex = Regex::new(&cli.path_exclude_pattern)?;
	let mut matches: Vec<FileLinesMatches> = Vec::new();
	for entry in WalkDir::new(&cli.search_path).follow_links(true) {
		let handle: &DirEntry = &entry?;
		if !handle.file_type().is_file() {
			// Is not a file => we're not interested...
			continue;
		}

		let file_path: &str = handle
			.path()
			.to_str()
			.ok_or("Unable to read file's path!")?;
		let exclude_file: bool = match cli.path_exclude_pattern.as_ref() {
			"" => false,
			_ => exclude_regexp.is_match(file_path),
		};
		let include_file: bool = include_regexp.is_match(file_path);
		if !include_file || exclude_file {
			// User doesn't want program to consider these file's names/paths
			continue;
		}
		if verbosity_level > 1 as u64 {
			eprintln!("\tFound: {}", file_path);
		}

		let matched_lines = line_matching::find_contiguous_lines_matching(
			handle,
			&cli.regex_pattern,
			cli.minimum_lines,
			verbosity_level,
		)?;
		if matched_lines.get_matching_lines().is_empty() {
			// No matching lines...
			continue;
		}

		// Finally, all checks passed
		matches.push(matched_lines);
	}

	// Print results found:
	for matched_lines in matches {
		eprint!("\nWith regex pattern '{}', ", &cli.regex_pattern,);
		eprintln!(
            "the following lines (of which there were a minimum of {} consecutive/contiguous ones) were found in the file:",
            &cli.minimum_lines,
        );
		let mut previous_line_no: usize = 0;
		for (line_no, line) in matched_lines.get_matching_lines().iter() {
			if previous_line_no > 0 && line_no - 1 > previous_line_no {
				eprintln!("--");
			}
			println!("{}:{}:{}", &matched_lines.get_file_path(), line_no, line);
			previous_line_no = *line_no;
		}
	}

	Ok(())
}
