# `find_consecutive_lines_of_code`
Have you ever been frustrated over knowing you've got code (like the below) located one or more places in your source code?
```csharp
ObjectType Obj2.a = Obj1.a;
ObjectType Obj2.b = Obj1.b;
ObjectType Obj2.c = Obj1.c;
ObjectType Obj2.d = Obj1.d;
ObjectType Obj2.e = Obj1.e;
ObjectType Obj2.f = Obj1.f;
ObjectType Obj2.g = Obj1.g;
// ...etc...
```
But when you want to do something about it (like replacing the above code with the below), you don't remember where they're located?
```csharp
// Using AutoMapper: https://www.infoworld.com/article/3192900/how-to-work-with-automapper-in-c.html
var config = new MapperConfiguration(cfg => {
    cfg.CreateMap<ObjectType, ObjectType>()
    // Example code for more complicated conversion between different types:
    // .ForMember(
    //     destination => destination.ContactDetails,
    //    opts => opts.MapFrom(source => source.Contact)
    // );
});

iMapper.Map(Obj1, Obj2);
```

Fret not! `find_consecutive_lines_of_code` is here!

## Getting Started
### Requirements
1. `cargo`
2. `rustc` _2018_ edition.

### Installation
1. Git clone this repo.
2. In git repo root; `cargo install --path .`
   - Add `--force` flag if you've done so before, and want to ensure you've got latest.
3. Ensure `~/.cargo/bin` is on your `$PATH` variable (`PATH="$HOME/.cargo/bin:$PATH").
4. Find the folder with source code you wanna find matching lines in, and use the tool!
   - The `-h` (or `--help`) flags specify your options/document usage!

##  Build and Test
1. Execute `cargo build` in repo-root.

### Tests
Unfortunately lacking =,(....
